//
//  Device+CoreDataClass.swift
//  HeartRateMonitor
//
//  Created by Nikita Koniukh on 21/04/2020.
//  Copyright © 2019 Nikita Koniukh. All rights reserved.
//
//

import UIKit
import CoreData

@objc(Device)
public class Device: NSManagedObject {

  public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
    super.init(entity: entity, insertInto: context)
  }

  init(name: String) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let description = NSEntityDescription.entity(forEntityName: "Device", in: context)!

    super.init(entity: description, insertInto: context)

    self.name = name
  }

}

extension NSObject{
    var context: NSManagedObjectContext{
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
}
