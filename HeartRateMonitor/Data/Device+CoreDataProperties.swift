//
//  Device+CoreDataProperties.swift
//  HeartRateMonitor
//
//  Created by Nikita Koniukh on 21/04/2020.
//  Copyright © 2019 Nikita Koniukh. All rights reserved.
//
//

import Foundation
import CoreData


extension Device {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Device> {
        return NSFetchRequest<Device>(entityName: "Device")
    }

    @NSManaged public var name: String?

}
