
import UIKit
import CoreBluetooth
import CoreData

class HRMViewController: UIViewController {

  @IBOutlet var tableView: UITableView!
  @IBOutlet var numberOfCurrentDevices: UILabel!
  @IBOutlet var userUniqId: UILabel!
    

  let coronaNearByCBUUID = CBUUID(string: "0xB81D")
  let serviceUUID = CBUUID(string:"0000b81d-0000-1000-8000-00805f9b34fb")
  private var service: CBUUID!
  private var value = "B81D"
  
  var devices = [Device]()
  
  var centralManager: CBCentralManager!
  var peripheralManager: CBPeripheralManager!

  override func viewDidLoad() {
    super.viewDidLoad()
    userUniqId.text = "\(UIDevice.current.identifierForVendor!)"
    centralManager = CBCentralManager(delegate: self, queue: nil)
    peripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
    devices = fetchDevice()
  }

  @IBAction func clearDbButtonWasPressed(_ sender: UIButton) {
    numberOfCurrentDevices.text = "\(0)"
    devices.removeAll()
    deleteAllDevices()
    print(devices.count)
    tableView.reloadData()
  }

  //MARK: - Core Data Methods
  func addNewDevice(device: Device){
    self.devices.append(device)
    do{
      try self.context.save()
    }catch{
      fatalError("Fail\(error)")
    }
  }

  func fetchDevice()->[Device]{
    let request: NSFetchRequest<Device> = Device.fetchRequest()
    do{
      let result = try context.fetch(request)
      print("fetchet req: ", result.count)
      return result
    }catch{
      fatalError("Fail\(error)")
    }
  }

  func deleteAllDevices(){
    let request: NSFetchRequest<Device> = Device.fetchRequest()
    do{
      let result = try context.fetch(request)
      for object in result {
        context.delete(object)
      }
      try context.save()
      print("db.count: ", result.count)
    }catch{
      fatalError("Fail\(error)")
    }
  }
}

extension HRMViewController : CBCentralManagerDelegate, CBPeripheralManagerDelegate {
  func startAdvertising() {
    peripheralManager.startAdvertising([CBAdvertisementDataLocalNameKey : "CoronaNearBy", CBAdvertisementDataServiceUUIDsKey : [serviceUUID]])
  }
  
  func addServices() {
    // 1. Create instance of CBMutableCharcateristic
      let myChar1 = CBMutableCharacteristic(type: coronaNearByCBUUID, properties: [.notify, .write, .read], value: nil, permissions: [.readable, .writeable])
      // 2. Create instance of CBMutableService
      let myService = CBMutableService(type: serviceUUID, primary: true)
      // 3. Add characteristics to the service
      myService.characteristics = [myChar1]
      // 4. Add service to peripheralManager
      peripheralManager.add(myService)
      // 5. Start advertising
      startAdvertising()
  }
  
  func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
    switch peripheral.state {
    case .unknown:
      print("Bluetooth Device is UNKNOWN")
    case .unsupported:
      print("Bluetooth Device is UNSUPPORTED")
    case .unauthorized:
      print("Bluetooth Device is UNAUTHORIZED")
    case .resetting:
      print("Bluetooth Device is RESETTING")
    case .poweredOff:
      print("Bluetooth Device is POWERED OFF")
    case .poweredOn:
      print("Bluetooth Device is POWERED ON")
      addServices()
    @unknown default:
      print("Unknown State")
    }
  }
  
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    switch central.state {
    case .unknown:
      print("central.state is .unknown")
    case .resetting:
      print("central.state is .resetting")
    case .unsupported:
      print("central.state is .unsupported")
    case .unauthorized:
      print("central.state is .unauthorized")
    case .poweredOff:
      print("central.state is .poweredOff")
    case .poweredOn:
      print("central.state is .poweredOn")
            Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (t) in
              self.centralManager.scanForPeripherals(withServices: [self.coronaNearByCBUUID])
            }
    }
  }
  func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                      advertisementData: [String: Any], rssi RSSI: NSNumber) {
    // add new device
    if !devices.contains(where: { $0.name == "\(peripheral.identifier)"}) {
      let newDevice = Device(name: "\(peripheral.identifier)")
      addNewDevice(device: newDevice)
    } else {
      print("found")
    }
    print("devices count: ", devices.count)
    tableView.reloadData()
    numberOfCurrentDevices.text = "\(devices.count)"
  }
}

extension HRMViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return devices.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "deviceCell", for: indexPath)
    let savedDevice = devices[indexPath.row]
    cell.textLabel?.text = savedDevice.name
    return cell
  }

}


